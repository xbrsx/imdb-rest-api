﻿using AutoMapper;
using IMDb.API.Extensions;
using IMDb.API.ViewModels;
using IMDb.API.ViewModels.Movie;
using IMDb.API.ViewModels.User;
using IMDb.Domain.Models;
using IMDb.Domain.Models.DTO;

namespace IMDb.API.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<ActorViewModel, Actor>().ReverseMap();
            CreateMap<ActorMovieViewModel, ActorMovie>().ReverseMap();
            CreateMap<DirectorViewModel, Director>().ReverseMap();
            CreateMap<GenreViewModel, Genre>().ReverseMap();
            CreateMap<MovieGenreViewModel, MovieGenre>().ReverseMap();
            CreateMap<MovieViewModel, Movie>().ReverseMap();
            CreateMap<RateMovieViewModel, RateMovie>().ReverseMap();

            CreateMap<RegisterMovieViewModel, Movie>().ReverseMap();
            CreateMap<RegisterMovieGenre, MovieGenre>().ReverseMap();
            CreateMap<RegisterMovieActor, ActorMovie>().ReverseMap();
            
            CreateMap<FilteredMovieViewModel, FilteredMovieDTO>().ReverseMap();
            CreateMap<UserViewModel, IMDbUser>().ReverseMap();

            CreateMap<EvaluateMovieViewModel, EvaluateMovieDTO>().ReverseMap();

            CreateMap<PaginatedViewModel<FilteredMovieViewModel>, PaginatedDTO<FilteredMovieDTO>>().ReverseMap();
        }
    }
}
