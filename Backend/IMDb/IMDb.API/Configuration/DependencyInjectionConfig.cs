﻿using IMDb.API.Extensions;
using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Interfaces.Services;
using IMDb.Domain.Interfaces.User;
using IMDb.Domain.Notifications;
using IMDb.Domain.Services;
using IMDb.Infrastructure.Data;
using IMDb.Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace IMDb.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(x => new SqlContext(new DbContextOptionsBuilder<SqlContext>()
                .UseSqlServer(configuration.GetConnectionString("DefaultConnection")).Options));

            services.AddScoped<IActorRepository, ActorRepository>();
            services.AddScoped<IGenreRepository, GenreRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IRateMovieRepository, RateMovieRepository>();

            services.AddScoped<INotificator, Notificator>();
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IRateMovieService, RateMovieService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUser, AspNetUser>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}
