﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace IMDb.API.Extensions
{
    public class IMDbUser : IdentityUser
    {
        [Required]
        public string Name { get; set; }
        public bool Active { get; set; } = true;
    }
}
