﻿using AutoMapper;
using IMDb.API.Controllers;
using IMDb.API.ViewModels;
using IMDb.API.ViewModels.Movie;
using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Services;
using IMDb.Domain.Interfaces.User;
using IMDb.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IMDb.API.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class MovieController : MainController
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;
        public MovieController(
            INotificator notificator, 
            IUser user,
            IMovieService movieService,
            IMapper mapper) : base(notificator, user)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> RegisterMovie(RegisterMovieViewModel registerMovieViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var result = await _movieService.RegisterMovie(_mapper.Map<Movie>(registerMovieViewModel));

            return CustomResponse(result);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, User")]
        public async Task<IActionResult> GetMovies(
            [FromQuery] int page = 1,
            [FromQuery] int pageSize = 5,
            [FromQuery] string[] directors = null,
            [FromQuery] string[] genres = null,
            [FromQuery] string[] actors = null)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var result = await _movieService.GetMoviesByFilter(
                page,
                pageSize,
                directors,
                genres,
                actors);

            var paginatedViewModel = _mapper.Map<PaginatedViewModel<FilteredMovieViewModel>>(result);

            return CustomResponse(paginatedViewModel);
        }
    }
}
