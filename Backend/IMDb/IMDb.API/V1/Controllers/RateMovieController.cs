﻿using AutoMapper;
using IMDb.API.Controllers;
using IMDb.API.ViewModels.Movie;
using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Services;
using IMDb.Domain.Interfaces.User;
using IMDb.Domain.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IMDb.API.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class RateMovieController : MainController
    {
        private readonly IRateMovieService _rateMovieService;
        private readonly IMapper _mapper;
        public RateMovieController(
            INotificator notificator, 
            IUser user,
            IRateMovieService rateMovieService,
            IMapper mapper) : base(notificator, user)
        {
            _rateMovieService = rateMovieService;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> RateMovie(EvaluateMovieViewModel evaluateMovieViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var result = await _rateMovieService.RateMovie(_mapper.Map<EvaluateMovieDTO>(evaluateMovieViewModel));

            return CustomResponse(result);
        }
    }
}
