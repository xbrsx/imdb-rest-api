﻿using IMDb.API.Controllers;
using IMDb.API.Extensions;
using IMDb.API.ViewModels;
using IMDb.API.ViewModels.User;
using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.User;
using IMDb.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IMDb.API.V1.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserController : MainController
    {
        private readonly SignInManager<IMDbUser> _signInManager;
        private readonly UserManager<IMDbUser> _userManager;
        private readonly AppSettings _appSettings;

        public UserController(
            INotificator notificator, 
            IUser user,
            SignInManager<IMDbUser> signInManager,
            UserManager<IMDbUser> userManager,
            IOptions<AppSettings> appSettings) : base(notificator, user)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _appSettings = appSettings.Value;
        }

        [HttpGet("GetCommomUsers")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> GetCommomUsers(int page = 1, int pageSize = 5)
        {
            PaginatedViewModel<UserViewModel> paginatedResult;

            if (page <= 0 || pageSize <= 0)
            {
                NotifyError("The paginated parameters must be greater than 0");
                return CustomResponse();
            }

            var commomUsers = await _userManager.GetUsersInRoleAsync(Roles.USER);

            if (commomUsers == null || !commomUsers.Any())
            {
                NotifyError("No users were found");
                return CustomResponse();
            }

            var totalSize = commomUsers.Count;

            commomUsers = commomUsers.OrderBy(x => x.Name).ToList();
            var commomUsersViewModel = commomUsers.Select(x => new UserViewModel { Email = x.Email, Name = x.Name, Id = x.Id });

            if(page == 1)
            {
                paginatedResult = new PaginatedViewModel<UserViewModel>(totalSize, page, pageSize, commomUsersViewModel.Take(pageSize), commomUsersViewModel);
                return CustomResponse(paginatedResult);
            }

            var size = pageSize * page;
            paginatedResult = new PaginatedViewModel<UserViewModel>(totalSize, page, pageSize, commomUsersViewModel.Take(size).Skip(size - pageSize), commomUsersViewModel);

            return CustomResponse(paginatedResult);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterUserViewModel registerUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var createdUser = await RegisterNewUser(registerUser, Roles.USER);

            if (createdUser != null)
            {
                await _signInManager.SignInAsync(createdUser, false);
                return CustomResponse(await BuildJWT(createdUser));
            }

            return CustomResponse(registerUser);
        }

        [HttpPost("RegisterAdministrator")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> RegisterAdministrator(RegisterUserViewModel registerUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var createdUser = await RegisterNewUser(registerUser, Roles.ADMINISTRATOR);

            if(createdUser != null)
            {
                await _signInManager.SignInAsync(createdUser, false);
                return CustomResponse(await BuildJWT(createdUser));
            }
            
            return CustomResponse(registerUser);
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginUserViewModel loginUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var result = await _signInManager.PasswordSignInAsync(loginUser.Email, loginUser.Password, false, true);
            
            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(loginUser.Email);

                if (!user.Active)
                {
                    NotifyError("This user is not active in our system");
                    return CustomResponse(loginUser);
                }

                return CustomResponse(await BuildJWT(user));
            }
            else if (result.IsLockedOut)
            {
                NotifyError("Usuário temporariamente bloqueado por tentativas inválidas");
                return CustomResponse(loginUser);
            }

            NotifyError("Usuário ou Senha incorretos");
            return CustomResponse(loginUser);
        }


        [HttpPut]
        [Authorize(Roles = "Administrator, User")]
        public async Task<IActionResult> Put(UserViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return CustomResponse(ModelState);
            }
            else if (!_user.IsInRole(Roles.ADMINISTRATOR) && _user.GetUserId().ToString() != userViewModel.Id)
            {
                NotifyError("You can't modify another person's account");
                return CustomResponse();
            }
            
            var user = await _userManager.FindByIdAsync(userViewModel.Id);

            if (user == null)
            {
                NotifyError("We can't find the user for this User Id");
                return CustomResponse();
            }
            else if(user.NormalizedEmail != userViewModel.Email.ToUpper())
            {
                var userWithSameEmail = await _userManager.FindByEmailAsync(userViewModel.Email);

                if(userWithSameEmail != null)
                {
                    NotifyError("This E-mail is already being used by another user");
                    return CustomResponse();
                }
            }

            user.Email = userViewModel.Email;
            user.UserName = userViewModel.Email;
            user.Name = userViewModel.Name;

            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                result.Errors.Select(x => {
                    NotifyError(x.Description);
                    return x;
                });

                return CustomResponse();
            }

            return CustomResponse();
        }

        [HttpDelete("{userId}")]
        [Authorize(Roles = "Administrator, User")]
        public async Task<IActionResult> Delete(string userId)
        {
            if (string.IsNullOrEmpty(userId.ToString()))
            {
                NotifyError("UserId cannot be empty");
                return CustomResponse();
            }
            else if (!_user.IsInRole(Roles.ADMINISTRATOR) && _user.GetUserId().ToString() != userId)
            {
                NotifyError("You can't delete another person's account");
                return CustomResponse();
            }

            var user = await _userManager.FindByIdAsync(userId.ToString());

            if(user == null)
            {
                NotifyError("We can't find the user for this User Id");
                return CustomResponse();
            }
            else if (!user.Active)
            {
                NotifyError("This user is already deactivated");
                return CustomResponse();
            }

            user.Active = false;
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                result.Errors.Select(x => {
                    NotifyError(x.Description); 
                    return x; 
                });

                return CustomResponse();
            }

            return CustomResponse();
        }

        private async Task<IMDbUser> RegisterNewUser(RegisterUserViewModel registerUser, params string[] roles)
        {
            var user = new IMDbUser
            {
                Name = registerUser.Name,
                UserName = registerUser.Email,
                Email = registerUser.Email,
                EmailConfirmed = true,
                Active = true,
            };

            var result = await _userManager.CreateAsync(user, registerUser.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    NotifyError(error.Description);
                }

                return null;
            }
            else if(roles != null && roles.Any())
            {
                await _userManager.AddToRolesAsync(user, roles);
            }

            return user;           
        }

        private async Task<LoginResponseViewModel> BuildJWT(IMDbUser user)
        {
            var claims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(DateTime.UtcNow).ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64));

            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim("role", userRole));
            }

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _appSettings.Emitter,
                Audience = _appSettings.ValidIn,
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(_appSettings.ExpirationHours),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            var encodedToken = tokenHandler.WriteToken(token);

            var response = new LoginResponseViewModel
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromHours(_appSettings.ExpirationHours).TotalSeconds,
                UserToken = new UserTokenViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Claims = claims.Select(c => new ClaimViewModel { Type = c.Type, Value = c.Value })
                }
            };

            return response;
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}
