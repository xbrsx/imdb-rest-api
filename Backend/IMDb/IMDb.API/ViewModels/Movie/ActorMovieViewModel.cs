﻿namespace IMDb.API.ViewModels.Movie
{
    public class ActorMovieViewModel
    {
        public string Character { get; set; }
        public string ActorId { get; set; }
        public ActorViewModel Actor { get; set; }
        public string MovieId { get; set; }
        public MovieViewModel Movie { get; set; }
    }
}
