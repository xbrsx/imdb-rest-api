﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.Movie
{
    public class ActorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ActorMovieViewModel> Movies { get; set; }
    }
}
