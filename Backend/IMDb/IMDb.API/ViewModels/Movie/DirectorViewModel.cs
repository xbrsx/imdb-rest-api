﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.Movie
{
    public class DirectorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<MovieViewModel> Movies { get; set; }
    }
}
