﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IMDb.API.ViewModels.Movie
{
    public class EvaluateMovieViewModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid MovieId { get; set; }

        [Required]
        [Range(0, 4)]
        public int Stars { get; set; }
    }
}
