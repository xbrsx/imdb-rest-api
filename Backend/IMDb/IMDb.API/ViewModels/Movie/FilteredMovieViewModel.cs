﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.Movie
{
    public class FilteredMovieViewModel
    {
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public DirectorViewModel Director { get; set; }
        public float Rate { get; set; }
        public virtual ICollection<GenreViewModel> Genres { get; set; }
        public virtual ICollection<ActorViewModel> Actors { get; set; }
        public virtual ICollection<RateMovieViewModel> RateMovies { get; set; }
    }
}
