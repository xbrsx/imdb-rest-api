﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.Movie
{
    public class GenreViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieGenreViewModel> Movies { get; set; }
    }
}
