﻿namespace IMDb.API.ViewModels.Movie
{
    public class MovieGenreViewModel
    {
        public string GenreId { get; set; }
        public GenreViewModel Genre { get; set; }
        public string MovieId { get; set; }
        public MovieViewModel Movie { get; set; }
    }
}
