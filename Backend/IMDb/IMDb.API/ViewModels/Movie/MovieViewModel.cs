﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.Movie
{
    public class MovieViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public DirectorViewModel Director { get; set; }
        public string DirectorId { get; set; }
        public virtual ICollection<MovieGenreViewModel> Genres { get; set; }
        public virtual ICollection<ActorMovieViewModel> Actors { get; set; }
        public virtual ICollection<RateMovieViewModel> Rates { get; set; }
    }
}
