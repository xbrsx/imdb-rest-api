﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IMDb.API.ViewModels.Movie
{
    public class RateMovieViewModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string MovieId { get; set; }
        
        public MovieViewModel Movie { get; set; }

        [Required]
        [Range(0,4)]
        public int Stars { get; set; }
    }
}
