﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDb.API.ViewModels.Movie
{
    public class RegisterMovieViewModel
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string Synopsis { get; set; }
        
        [Required]
        public Guid DirectorId { get; set; }

        [Required]
        [MinLength(1)]
        public virtual ICollection<RegisterMovieGenre> Genres { get; set; }
        
        [Required]
        [MinLength(1)]
        public virtual ICollection<RegisterMovieActor> Actors { get; set; }
    }

    public class RegisterMovieGenre
    {
        public Guid GenreId { get; set; }
    }

    public class RegisterMovieActor
    {
        public string Character { get; set; }
        public Guid ActorId { get; set; }
    }
}
