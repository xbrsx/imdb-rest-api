﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels
{
    public sealed class PaginatedViewModel<T>
    {       
        public int TotalSize { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<T> Data { get; set; }
        public IEnumerable<T> FilteredData { get; set; }
        public PaginatedViewModel(int totalSize, int page, int pageSize, IEnumerable<T> filteredData, IEnumerable<T> data)
        {
            TotalSize = totalSize;
            Page = page;
            PageSize = pageSize;
            Data = data;
            FilteredData = filteredData;
        }
    }
}
