﻿namespace IMDb.API.ViewModels.User
{
    public class ClaimViewModel
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
