﻿using System.ComponentModel.DataAnnotations;

namespace IMDb.API.ViewModels.User
{
    public class LoginUserViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }
    }
}
