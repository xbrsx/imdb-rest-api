﻿using System.Collections.Generic;

namespace IMDb.API.ViewModels.User
{
    public class UserTokenViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public IEnumerable<ClaimViewModel> Claims { get; set; }
    }
}
