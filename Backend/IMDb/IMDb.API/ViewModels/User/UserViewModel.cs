﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IMDb.API.ViewModels.User
{
    public class UserViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
