Olá :D

Segue algumas informações importantes sobre a API:

- Fazendo uso do DDD.
- Conceitos de SOLID e Clean Code aplicados.
- Injeção de Dependência aplicado.
- Documentação com Swagger.
- Utilização do Identity com JWT.
- AutoMapper utilizado.
- Entity Framework Core utilizado.
- SQL Server utilizado no servidor AWS.
- Validações dos modelos de domínio com FluentValidator.
- Notificator que cruza todas as camadas.
- Versionamento de API.
- API em padrão REST.
- Design Patterns aplicados (Ex: Repository & Singleton).
- Code First utilizando Migrations.
- ER Model desenhado no BrModelo.

Como o Banco de dados está hospeado em nuvem AWS a API está 
em plug and play, basta executar e fazer as requests.

Usuários:

- Administrador:
User: admin@ioasis.com
Password: Aa.123

- Usuário:
User: user@ioasis.com
Password: Aa.123

Já alguns dados previamente cadastrados.

Sobre os Extras:

- Testes = Infelizmente não tive tempo de escreve-los devido ao prazo de entrega, 
mas numa situação comum eu teria escrito.

- Sonarqube = utilizo atualmente em minha empresa atual.

- Docker = Habilitei o suporte da aplicação para Docker, porém não tenho muita experiência
com configuração de Containers.
