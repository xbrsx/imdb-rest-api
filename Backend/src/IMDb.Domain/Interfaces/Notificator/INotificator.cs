﻿using IMDb.Domain.Notifications;
using System.Collections.Generic;

namespace IMDb.Domain.Interfaces.Notificator
{
    public interface INotificator
    {
        bool HasNotifications();
        List<Notification> GetNotifications();
        void Handle(Notification notificacao);
    }
}
