﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Repositories
{
    public interface IActorRepository : IRepositoryBase<Actor>
    {
    }
}
