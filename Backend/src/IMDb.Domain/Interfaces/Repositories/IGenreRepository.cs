﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Repositories
{
    public interface IGenreRepository : IRepositoryBase<Genre>
    {
    }
}
