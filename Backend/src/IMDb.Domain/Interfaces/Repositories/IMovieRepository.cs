﻿using IMDb.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDb.Domain.Interfaces.Repositories
{
    public interface IMovieRepository : IRepositoryBase<Movie>
    {
        Task<IEnumerable<Movie>> GetAllIncluded();
    }
}
