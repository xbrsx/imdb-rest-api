﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Repositories
{
    public interface IRateMovieRepository : IRepositoryBase<RateMovie>
    {
    }
}
