﻿using IMDb.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDb.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : Entity
    {
        Task InsertAsync(TEntity entity);
        Task<TEntity> GetByIdAsync(params Guid[] ids);
        Task<List<TEntity>> GetAllAsync();
        void Update(TEntity entity);
        void Remove(TEntity entity);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<bool> Commit();
    }
}
