﻿using IMDb.Domain.Models;
using IMDb.Domain.Models.DTO;
using System.Threading.Tasks;

namespace IMDb.Domain.Interfaces.Services
{
    public interface IMovieService
    {
        Task<bool> RegisterMovie(Movie movie);
        Task<PaginatedDTO<FilteredMovieDTO>> GetMoviesByFilter(int page, int pageSize, string[] directors, string[] genres, string[] actors);
    }
}
