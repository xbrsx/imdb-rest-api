﻿using IMDb.Domain.Models;
using IMDb.Domain.Models.DTO;
using System.Threading.Tasks;

namespace IMDb.Domain.Interfaces.Services
{
    public interface IRateMovieService
    {
        Task<bool> RateMovie(EvaluateMovieDTO evaluateMovie);
    }
}
