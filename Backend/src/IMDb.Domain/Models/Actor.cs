﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Actor : Entity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ActorMovie> Movies { get; set; }
    }
}
