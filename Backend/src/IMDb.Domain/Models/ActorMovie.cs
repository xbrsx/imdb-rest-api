﻿using System;

namespace IMDb.Domain.Models
{
    public class ActorMovie : Entity
    {
        public string Character { get; set; }
        public Guid ActorId { get; set; }
        public Actor Actor { get; set; }
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
