﻿using System;

namespace IMDb.Domain.Models.DTO
{
    public class EvaluateMovieDTO
    {
        public Guid UserId { get; set; }
        public Guid MovieId { get; set; }
        public int Stars { get; set; }
    }
}
