﻿using System.Collections.Generic;
using System.Linq;

namespace IMDb.Domain.Models.DTO
{
    public class FilteredMovieDTO
    {
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public Director Director { get; set; }
        public float Rate { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
        public virtual ICollection<Actor> Actors { get; set; }
        public virtual ICollection<RateMovie> RateMovies { get; set; }

        public FilteredMovieDTO(
            string name,
            string synopsis,
            Director director,
            ICollection<Genre> genres,
            ICollection<Actor> actors,
            ICollection<RateMovie> rateMovies)
        {
            Name = name;
            Synopsis = synopsis;
            Director = director;
            Director.Movies = null;
            Genres = genres.Select(x => { x.Movies = null; return x; }).ToList();
            Actors = actors.Select(x => { x.Movies = null; return x; }).ToList();
            RateMovies = rateMovies.Select(x => { x.Movie = null; return x; }).ToList();
            Rate = rateMovies.Count <= 0 ? 0 : rateMovies.Sum(x => x.Stars) / rateMovies.Count;
        }
    }
}
