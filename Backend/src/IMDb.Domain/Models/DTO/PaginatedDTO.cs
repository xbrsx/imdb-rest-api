﻿using System.Collections.Generic;

namespace IMDb.Domain.Models.DTO
{
    public class PaginatedDTO<T>
    {
        public int TotalSize { get; private set; }
        public int Page { get; private set; }
        public int PageSize { get; private set; }
        public IEnumerable<T> FilteredData { get; private set; }
        public IEnumerable<T> Data { get; private set; }
        public PaginatedDTO(int totalSize, int page, int pageSize, IEnumerable<T> filteredData, IEnumerable<T> data)
        {
            TotalSize = totalSize;
            Page = page;
            PageSize = pageSize;
            Data = data;
            FilteredData = filteredData;
        }
    }
}
