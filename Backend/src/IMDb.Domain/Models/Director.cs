﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Director : Entity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
