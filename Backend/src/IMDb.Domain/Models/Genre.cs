﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Genre : Entity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieGenre> Movies { get; set; }
    }
}
