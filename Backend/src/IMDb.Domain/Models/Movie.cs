﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Movie : Entity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public Director Director { get; set; }
        public Guid DirectorId { get; set; }
        public virtual ICollection<MovieGenre> Genres { get; set; }
        public virtual ICollection<ActorMovie> Actors { get; set; }
        public virtual ICollection<RateMovie> Rates { get; set; }
    }
}
