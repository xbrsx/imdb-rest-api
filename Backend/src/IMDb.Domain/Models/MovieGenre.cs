﻿using System;

namespace IMDb.Domain.Models
{
    public class MovieGenre : Entity
    {
        public Guid GenreId { get; set; }
        public Genre Genre { get; set; }
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
