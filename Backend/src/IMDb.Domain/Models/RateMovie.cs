﻿using System;

namespace IMDb.Domain.Models
{
    public class RateMovie : Entity
    {
        public Guid UserId { get; set; }
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
        public int Stars { get; set; }
    }
}
