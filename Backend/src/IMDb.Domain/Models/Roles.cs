﻿namespace IMDb.Domain.Models
{
    public static class Roles
    {
        public const string ADMINISTRATOR = "Administrator";
        public const string USER = "User";
    }
}
