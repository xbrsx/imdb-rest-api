﻿using IMDb.Domain.Interfaces.Notificator;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Domain.Notifications
{
    public class Notificator : INotificator
    {
        private readonly IList<Notification> _notifications;
        public Notificator()
        {
            _notifications = new List<Notification>();
        }
        public List<Notification> GetNotifications() => _notifications.ToList();

        public void Handle(Notification notificacao) => _notifications.Add(notificacao);
        public bool HasNotifications() => _notifications.Any();
    }
}
