﻿using FluentValidation;
using FluentValidation.Results;
using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Models;
using IMDb.Domain.Notifications;

namespace IMDb.Domain.Services
{
    public abstract class BaseService
    {
        private readonly INotificator _notificator;
        protected BaseService(INotificator notificator)
        {
            _notificator = notificator;
        }

        protected void Notify(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors)
            {
                Notify(error.ErrorMessage);
            }
        }

        protected void Notify(string message)
        {
            _notificator.Handle(new Notification(message));
        }

        protected bool ValidateEntity<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE> where TE : class
        {
            var validator = validacao.Validate(entidade);

            if (validator.IsValid) return true;

            Notify(validator);

            return false;
        }
    }
}
