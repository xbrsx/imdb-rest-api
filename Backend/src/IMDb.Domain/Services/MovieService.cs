﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Interfaces.Services;
using IMDb.Domain.Models;
using IMDb.Domain.Models.DTO;
using IMDb.Domain.Validations.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Domain.Services
{
    public class MovieService : BaseService, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IGenreRepository _genreRepository;
        public MovieService(
            INotificator notificator,
            IMovieRepository movieRepository,
            IActorRepository actorRepository,
            IGenreRepository genreRepository) : base(notificator)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _genreRepository = genreRepository;
        }

        public async Task<PaginatedDTO<FilteredMovieDTO>> GetMoviesByFilter(
            int page, 
            int pageSize, 
            string[] directors, 
            string[] genres, 
            string[] actors)
        {
            var movies = await _movieRepository.GetAllIncluded();

            movies = FilterByActors(movies, actors);
            movies = FilterByGenres(movies, genres);
            movies = FilterByDirectors(movies, directors);

            var filteredMovies = movies.Select(x => new FilteredMovieDTO(
                x.Name, 
                x.Synopsis, 
                x.Director, 
                x.Genres.Select(g => g.Genre).ToList(), 
                x.Actors.Select(a => a.Actor).ToList(), 
                x.Rates));


            filteredMovies = filteredMovies
                                        .OrderByDescending(x => x.RateMovies.Count)
                                        .ThenByDescending(x => x.Rate)
                                        .ThenBy(x => x.Name);                       

            if (!movies.Any())
            {
                Notify("No Movie was found in our system");
                return null;
            }

            var totalSize = movies.Count();

            if(page == 1)
            {
                return new PaginatedDTO<FilteredMovieDTO>(totalSize, page, pageSize, filteredMovies.Take(pageSize), filteredMovies);
            }

            var size = page * pageSize;
            return new PaginatedDTO<FilteredMovieDTO>(totalSize, page, pageSize, filteredMovies.Take(size).Skip(size - pageSize), filteredMovies);
        }

        public async Task<bool> RegisterMovie(Movie movie)
        {
            if (!ValidateEntity(new RegisterMovieValidation(), movie))
                return false;

            var actors = await _actorRepository.FindAsync(x => movie.Actors.Select(a => a.ActorId).Any(y => y == x.Id));
            var genres = await _genreRepository.FindAsync(x => movie.Genres.Select(a => a.GenreId).Any(y => y == x.Id));

            if(actors == null || actors.Count() != movie.Actors.Count)
            {
                Notify($"One or more Actors doesn't exist in our system");
                return false;
            }
            else if(genres == null || genres.Count() != movie.Genres.Count)
            {
                Notify($"One or more Genres doesn't exist in our system");
                return false;
            }

            await _movieRepository.InsertAsync(movie);

            return await _movieRepository.Commit();
        }

        private IEnumerable<Movie> FilterByDirectors(IEnumerable<Movie> movies, string[] directors)
        {
            if (directors == null || !directors.Any())
                return movies;

            return movies.Where(x => directors.Any(d => d.ToLower().Contains(x.Director.Name.ToLower())));
        }

        private IEnumerable<Movie> FilterByGenres(IEnumerable<Movie> movies, string[] genres)
        {
            if (genres == null || !genres.Any())
                return movies;

            return movies.Where(x => genres.Select(d => d.ToLower()).Any(y => x.Genres.Select(a => a.Genre.Name.ToLower()).Contains(y)));
        }

        private IEnumerable<Movie> FilterByActors(IEnumerable<Movie> movies, string[] actors)
        {
            if (actors == null || !actors.Any())
                return movies;

            
            return movies.Where(x => actors.Select(d => d.ToLower()).Any(y => x.Actors.Select(a => a.Actor.Name.ToLower()).Contains(y)));
        }
    }
}
