﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Interfaces.Services;
using IMDb.Domain.Interfaces.User;
using IMDb.Domain.Models;
using IMDb.Domain.Models.DTO;
using IMDb.Domain.Validations.Movies;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Domain.Services
{
    public class RateMovieService : BaseService, IRateMovieService
    {
        private readonly IRateMovieRepository _rateMovieRepository;
        private readonly IUser _user;
        public RateMovieService(
            INotificator notificator,
            IRateMovieRepository rateMovieRepository,
            IUser user) : base(notificator)
        {
            _rateMovieRepository = rateMovieRepository;
            _user = user;
        }

        public async Task<bool> RateMovie(EvaluateMovieDTO evaluateMovie)
        {
            if (!ValidateEntity(new RateMovieValidation(), evaluateMovie)) 
            {
                return false;
            }
            else if (_user.GetUserId() != evaluateMovie.UserId)
            {
                Notify("You cannot rate for another person");
                return false;
            }

            var ratedMovie = (await _rateMovieRepository.FindAsync(x => x.MovieId == evaluateMovie.MovieId && x.UserId == evaluateMovie.UserId))?.FirstOrDefault();

            if(ratedMovie != null)
            {
                ratedMovie.Stars = evaluateMovie.Stars;
                _rateMovieRepository.Update(ratedMovie);
                return await _rateMovieRepository.Commit();
            }

            await _rateMovieRepository.InsertAsync(new RateMovie(){
                MovieId = evaluateMovie.MovieId,
                Stars = evaluateMovie.Stars,
                UserId = evaluateMovie.UserId
            });

            return await _rateMovieRepository.Commit();
        }
    }
}
