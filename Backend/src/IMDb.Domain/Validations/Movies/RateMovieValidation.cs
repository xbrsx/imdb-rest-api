﻿using FluentValidation;
using IMDb.Domain.Models.DTO;

namespace IMDb.Domain.Validations.Movies
{
    public class RateMovieValidation : AbstractValidator<EvaluateMovieDTO>
    {
        public RateMovieValidation()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User ID cannot be empty");

            RuleFor(x => x.MovieId)
                .NotEmpty().WithMessage("User ID cannot be empty");

            RuleFor(x => x.Stars)
                .NotEmpty().WithMessage("Stars cannot be empty")
                .GreaterThanOrEqualTo(0).WithMessage("The minimum value for rate a movie is 0 stars")
                .LessThanOrEqualTo(4).WithMessage("The maximum value for rate a movie is 4 stars");
        }
    }
}
