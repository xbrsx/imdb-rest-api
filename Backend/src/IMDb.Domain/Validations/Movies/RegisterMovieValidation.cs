﻿using FluentValidation;
using IMDb.Domain.Models;

namespace IMDb.Domain.Validations.Movies
{
    public class RegisterMovieValidation : AbstractValidator<Movie>
    {
        public RegisterMovieValidation()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Movie name could not be empty")
                .MaximumLength(100).WithMessage("Movie name must have between 1 and 100 characters");

            RuleFor(x => x.Synopsis)
               .NotEmpty().WithMessage("Movie synopsis could not be empty")
               .MaximumLength(200).WithMessage("Movie synopsis must have between 1 and 200 characters");

            RuleFor(x => x.DirectorId)
                .NotEmpty().WithMessage("Movie director could not be empty");

            RuleFor(x => x.Genres)
               .NotEmpty().WithMessage("Movie must have at least one genre");

            RuleFor(x => x.Actors)
               .NotEmpty().WithMessage("Movie must have at least one actor");
        }
    }
}
