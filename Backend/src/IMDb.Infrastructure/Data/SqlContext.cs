﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace IMDb.Infrastructure.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext(DbContextOptions options) : base(options){}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SqlContext).Assembly);

            modelBuilder.Entity<Actor>().HasData(
                new Actor { Id = Guid.NewGuid(), Name = "Robert Pattinson" },
                new Actor { Id = Guid.NewGuid(), Name = "Tom Hanks" },
                new Actor { Id = Guid.NewGuid(), Name = "Scarlett Johansson" },
                new Actor { Id = Guid.NewGuid(), Name = "Leonardo DiCaprio" },
                new Actor { Id = Guid.NewGuid(), Name = "Kristen Stewart" },
                new Actor { Id = Guid.NewGuid(), Name = "Matthew McConaughey" }
            );

            modelBuilder.Entity<Director>().HasData(
                new Actor { Id = Guid.NewGuid(), Name = "Christopher Nolan" },
                new Actor { Id = Guid.NewGuid(), Name = "Steven Spielberg" },
                new Actor { Id = Guid.NewGuid(), Name = "Martin Scorsese" },
                new Actor { Id = Guid.NewGuid(), Name = "Quentin Tarantino" },
                new Actor { Id = Guid.NewGuid(), Name = "José Padilha" }
            );

            modelBuilder.Entity<Genre>().HasData(
                new Actor { Id = Guid.NewGuid(), Name = "Action" },
                new Actor { Id = Guid.NewGuid(), Name = "Suspense" },
                new Actor { Id = Guid.NewGuid(), Name = "Drama" },
                new Actor { Id = Guid.NewGuid(), Name = "Horror" },
                new Actor { Id = Guid.NewGuid(), Name = "Comedy" },
                new Actor { Id = Guid.NewGuid(), Name = "Romance" },
                new Actor { Id = Guid.NewGuid(), Name = "Fiction" },
                new Actor { Id = Guid.NewGuid(), Name = "Adventure" }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
