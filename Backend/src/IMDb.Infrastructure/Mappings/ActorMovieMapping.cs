﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infrastructure.Mappings
{
    public class ActorMovieMapping : IEntityTypeConfiguration<ActorMovie>
    {
        public void Configure(EntityTypeBuilder<ActorMovie> builder)
        {
            builder.ToTable("Actors_Movies");

            builder.HasKey(x => new { x.ActorId, x.MovieId });

            builder.Property(x => x.ActorId).IsRequired();
            builder.Property(x => x.MovieId).IsRequired();
            builder.Property(x => x.Character).IsRequired().HasMaxLength(100);

            builder.HasOne(x => x.Actor)
                .WithMany(x => x.Movies)
                .HasForeignKey(x => x.ActorId)
                .HasPrincipalKey(x => x.Id)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Actors)
                .HasForeignKey(x => x.MovieId)
                .HasPrincipalKey(x => x.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
