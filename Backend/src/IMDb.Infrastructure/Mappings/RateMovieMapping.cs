﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infrastructure.Mappings
{
    public class RateMovieMapping : IEntityTypeConfiguration<RateMovie>
    {
        public void Configure(EntityTypeBuilder<RateMovie> builder)
        {
            builder.ToTable("Rate_Movies");

            builder.HasKey(x => new { x.MovieId, x.UserId });

            builder.Property(x => x.UserId).IsRequired();
            builder.Property(x => x.MovieId).IsRequired();
            builder.Property(x => x.Stars).IsRequired();

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Rates)
                .HasForeignKey(x => x.MovieId)
                .HasPrincipalKey(x => x.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
