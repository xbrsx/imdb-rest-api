﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDb.Infrastructure.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Directors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genres",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Synopsis = table.Column<string>(maxLength: 200, nullable: false),
                    DirectorId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Directors_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "Directors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Actors_Movies",
                columns: table => new
                {
                    ActorId = table.Column<Guid>(nullable: false),
                    MovieId = table.Column<Guid>(nullable: false),
                    Character = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors_Movies", x => new { x.ActorId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_Actors_Movies_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Actors_Movies_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Movies_Genres",
                columns: table => new
                {
                    GenreId = table.Column<Guid>(nullable: false),
                    MovieId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies_Genres", x => new { x.GenreId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_Movies_Genres_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Movies_Genres_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rate_Movies",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    MovieId = table.Column<Guid>(nullable: false),
                    Stars = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rate_Movies", x => new { x.MovieId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Rate_Movies_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("a42d3497-56e6-48f9-9af7-de945b14822d"), "Robert Pattinson" },
                    { new Guid("27b19d8a-6d85-4c78-a96d-44bdc1a7d20b"), "Tom Hanks" },
                    { new Guid("ac73bcb3-0020-4a26-98d5-acaa84e53e4d"), "Scarlett Johansson" },
                    { new Guid("37ec605f-d044-4f4f-9144-d1ac24ec71a5"), "Leonardo DiCaprio" },
                    { new Guid("fbc848a2-cd03-4d9f-85b8-2c5405a2964d"), "Kristen Stewart" },
                    { new Guid("08a5bc60-494e-45f9-9837-b01c32cb5244"), "Matthew McConaughey" }
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("2c7071ee-026b-43ff-8d14-61561f5de1af"), "José Padilha" },
                    { new Guid("59bdf9c5-96d8-4b8b-acdd-b37fe616d488"), "Martin Scorsese" },
                    { new Guid("b0841f68-b543-4e12-b7d1-67cc9c79b5ea"), "Quentin Tarantino" },
                    { new Guid("9506cfeb-143b-4d6c-b8ff-aeab85fd90d4"), "Christopher Nolan" },
                    { new Guid("f4e0e6dd-3ac2-497a-b50c-d963ff122674"), "Steven Spielberg" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("fb47799e-9691-46d8-8223-59040f3b3e1e"), "Fiction" },
                    { new Guid("bc2dfe86-6e8e-47fa-aed9-71d3cd3a38c8"), "Action" },
                    { new Guid("0fe55fc1-7bc5-463f-9525-9bc7b79794f6"), "Suspense" },
                    { new Guid("6f7d6c63-8434-4b7c-a16a-0fab54d8f359"), "Drama" },
                    { new Guid("d3ea4c0a-71bb-4a14-b3e5-3199d80495a9"), "Horror" },
                    { new Guid("1799de64-ded5-44a5-a65c-f6fff7a311d6"), "Comedy" },
                    { new Guid("9ccf01cb-03be-41e4-9375-6f9ee1b8e4bf"), "Romance" },
                    { new Guid("a12aacbc-bc7c-45c0-bc05-6c6e93c40bd8"), "Adventure" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Actors_Movies_MovieId",
                table: "Actors_Movies",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_DirectorId",
                table: "Movies",
                column: "DirectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_Genres_MovieId",
                table: "Movies_Genres",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Actors_Movies");

            migrationBuilder.DropTable(
                name: "Movies_Genres");

            migrationBuilder.DropTable(
                name: "Rate_Movies");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Genres");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Directors");
        }
    }
}
