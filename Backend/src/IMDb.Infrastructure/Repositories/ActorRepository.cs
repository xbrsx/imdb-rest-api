﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Models;
using IMDb.Infrastructure.Data;

namespace IMDb.Infrastructure.Repositories
{
    public class ActorRepository : RepositoryBase<Actor>, IActorRepository
    {
        public ActorRepository(
            SqlContext context, 
            INotificator notificator) : base(context, notificator)
        {
        }
    }
}
