﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Models;
using IMDb.Infrastructure.Data;

namespace IMDb.Infrastructure.Repositories
{
    public class GenreRepository : RepositoryBase<Genre>, IGenreRepository
    {
        public GenreRepository(
            SqlContext context, 
            INotificator notificator) : base(context, notificator)
        {
        }
    }
}
