﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Models;
using IMDb.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDb.Infrastructure.Repositories
{
    public class MovieRepository : RepositoryBase<Movie>, IMovieRepository
    {
        public MovieRepository(
            SqlContext context, 
            INotificator notificator) : base(context, notificator)
        {
        }

        public async Task<IEnumerable<Movie>> GetAllIncluded()
        {
            return await _dbSet
                            .AsNoTracking()
                            .Include(x => x.Director)
                            .Include(x => x.Rates)
                            .Include(x => x.Genres).ThenInclude(x => x.Genre)
                            .Include(x => x.Actors).ThenInclude(x => x.Actor)
                            .ToListAsync();
                        
        }
    }
}
