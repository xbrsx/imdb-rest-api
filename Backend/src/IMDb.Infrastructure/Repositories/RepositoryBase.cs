﻿using IMDb.Domain.Interfaces.Notificator;
using IMDb.Domain.Interfaces.Repositories;
using IMDb.Domain.Models;
using IMDb.Domain.Notifications;
using IMDb.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDb.Infrastructure.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : Entity
    {
        protected readonly SqlContext _context;
        protected readonly INotificator _notificator;
        protected readonly DbSet<TEntity> _dbSet;
        protected RepositoryBase(
            SqlContext context,
            INotificator notificator)
        {
            _context = context;
            _notificator = notificator;
            _dbSet = _context.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
            => await _dbSet.AsNoTracking().Where(predicate).ToListAsync();

        public async Task<List<TEntity>> GetAllAsync()
            => await _dbSet.ToListAsync();

        public async Task<TEntity> GetByIdAsync(params Guid[] ids)
            => await _dbSet.FindAsync(ids);

        public async Task InsertAsync(TEntity entity)
            => await _dbSet.AddAsync(entity);

        public void Remove(TEntity entity)
            =>  _dbSet.Remove(entity);

        public void Update(TEntity entity)
            =>  _dbSet.Update(entity);

        public async Task<bool> Commit()
        {
            try
            {
                var affectedRows = await _context.SaveChangesAsync();
                return affectedRows > 0;
            }
            catch
            {
                _notificator.Handle(new Notification("Something went wrong trying to save data to database"));
                return false;
            }
        }

        public void Dispose()
            => _context?.Dispose();
        
    }
}
